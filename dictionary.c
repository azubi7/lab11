#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	int arraylength = 10;// Allocate memory for an array of strings (arr).
	char **arr = malloc(arraylength * sizeof(char*));
	int entries = 0;
	char line[20];
	while(fgets(line, 20,in) != NULL)
	{
		char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
		if(entries == arraylength)
		{
			arraylength += 10;
			arr = realloc(arr, arraylength*sizeof(char*));
		}
		char *str = malloc(20 * sizeof(char));
		//char *new = strcpy(str, line);
		strcpy(str, line);
		arr[entries]= str;
		entries++;
	}
	
	// Read the dictionary line by line.
	// Expand array if necessary (realloc).
	// Allocate memory for the string (str).
	// Copy each line into the string (use strcpy).
	// Attach the string to the large array (assignment =)
	*size = entries;
	
	// Return pointer to the array of strings.
	return arr;
	
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}